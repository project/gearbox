<?php


namespace Drupal\gearbox;


use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\PhpStorage\PhpStorageFactory;

class GearboxServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container) {
    PhpStorageFactory::get('gearbox')->deleteAll();
  }
}
