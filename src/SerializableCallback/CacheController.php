<?php

namespace Drupal\gearbox\SerializableCallback;

use Drupal;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use ReflectionFunction;
use ReflectionMethod;

class CacheController extends BaseSerializableCallback {

  protected array $namespaces = [
    NestedArray::class,
  ];

  private $cache;

  private array $controller = [];

  private bool $is_service = FALSE;

  private $controller_reflection;

  private array $controller_parameters = [];

  private ?array $response;

  /**
   * CacheController constructor.
   *
   * @param $cache
   * @param string|array $controller
   * @param array|null $response
   *
   * @throws \ReflectionException
   */
  public function __construct($cache, $controller, ?array $response = NULL) {
    parent::__construct();
    $this->cache = $cache;
    $this->response = $response;
    if (!$controller) {
      return;
    }
    if (is_string($controller) && strpos($controller, '::') !== FALSE) {
      $controller = explode('::', $controller);
    }
    if (is_string($controller) && strpos($controller, ':') !== FALSE) {
      $controller = explode(':', $controller);
      $this->is_service = TRUE;
    }
    $this->controller = $controller;
    if (is_array($controller)) {
      if ($this->is_service) {
        $controller_reflection = new ReflectionMethod(Drupal::service($controller[0]), $controller[1]);
      }
      else {
        $controller_reflection = new ReflectionMethod($controller[0], $controller[1]);
      }
    }
    else {
      $controller_reflection = new ReflectionFunction($controller);
    }
    $this->controller_reflection = $controller_reflection;
    foreach ($controller_reflection->getParameters() as $parameter) {
      $parameter_str = '';
      if ($parameter->hasType()) {
        $parameter_str .= $parameter->getType()->getName() . ' ';
      }
      if ($parameter->isPassedByReference()) {
        $parameter_str .= '&';
      }
      $parameter_str .= '$' . $parameter->getName();
      $parameter_str .= $parameter->isDefaultValueAvailable() ? (' = ' . var_export($parameter->getDefaultValue(), TRUE)) : '';
      $this->controller_parameters['$' . $parameter->getName()] = $parameter_str;
    }
  }

  /**
   * @return string
   */
  public function getFunctionCode(): string {
    $code = [];
    $lazy_response = !!$this->response && !!$this->cache;
    $code[] = "function {$this->getFunctionName()}(" . implode(', ', $this->controller_parameters) . '): array {';
    $code[] = '  return ' . $this->parseOperand(
        ($this->controller ? ['#controller_parameters' => array_keys($this->controller_parameters)] : [])
        + (($this->controller || $lazy_response) ? ['#pre_render' => [$this->getFunctionName('__prerender')]] : [])
        + ($this->response ? (!$lazy_response ? $this->response : []) : [])
        + ($this->cache ? [
          '#cache'            => $this->cache,
          '#cache_properties' => ['#breadcrumb'],
        ] : []),
        '  ') . ';';
    $code[] = '}';
    if ($this->controller || $lazy_response) {
      $code[] = '';
      $code[] = "function {$this->getFunctionName('__prerender')}(array \$response): array {";
      if ($lazy_response) {
        $code[] = "  return NestedArray::mergeDeep(\$response, {$this->parseOperand($this->response)});";
      }
      else {
        if (is_array($this->controller)) {
          $this->controller[0] = trim($this->controller[0], '\\');
        }
        if (is_array($this->controller) && is_subclass_of($this->controller[0], ContainerInjectionInterface::class)) {
          $code[] = "  \$controller = [{$this->controller[0]}::create(Drupal::getContainer()), {$this->parseOperand($this->controller[1])}];";
        }
        elseif ($this->is_service) {
          $code[] = "  \$controller = [Drupal::service({$this->parseOperand($this->controller[0])}), {$this->parseOperand($this->controller[1])}];";
        }
        else {
          $code[] = "  \$controller = {$this->parseOperand($this->controller)};";
        }
        $code[] = "  \$controller_parameters = \$response['#controller_parameters'];";
        $code[] = "  unset(\$response['#controller_parameters']);";
        $code[] = "  return NestedArray::mergeDeep(\$response, call_user_func_array(\$controller, \$controller_parameters));";
      }
      $code[] = '}';
    }

    return implode("\n", $code);
  }

}
