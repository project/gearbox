<?php

namespace Drupal\gearbox\SerializableCallback;

interface SerializableCallbackInterface {

  public function getFunctionName(string $suffix = ''): string;

  public function getFunctionCode(): string;

  /**
   * @return string[]
   */
  public function getNamespaces(): array;

}
