<?php

namespace Drupal\gearbox\SerializableCallback;

abstract class BaseSerializableCallback implements SerializableCallbackInterface {

  protected static int $function_count = 0;

  protected array $namespaces = [];

  private int $function_number;

  private string $function_name;

  /**
   * BaseSerializableCallback constructor.
   */
  function __construct() {
    $this->function_name = 'f__' . str_replace('\\', '_', static::class);
    do {
      $this->function_number = $this::$function_count++;
      $suffix = '_' . str_pad($this->function_number, 3, "0", STR_PAD_LEFT);
    } while (function_exists($this->function_name . $suffix));
    $this->function_name .= $suffix;
  }

  /**
   * @return array
   */
  public function getNamespaces(): array {
    return $this->namespaces;
  }

  /**
   * @param string $suffix
   *
   * @return string
   */
  public function getFunctionName(string $suffix = ''): string {
    return $this->function_name . $suffix;
  }

  /**
   * @param $op
   * @param string $space
   *
   * @return string
   */
  protected function parseOperand($op, string $space = '  '): string {
    if (is_string($op)) {
      if ($op[0] === '!') {
        return '!' . $this->parseOperand(mb_substr($op, 1), $space);
      }
      if ($op[0] !== '@' && $op[0] !== '$') {
        $op = var_export(preg_replace('#^\\\\(@|\$)#', '$1', $op), TRUE);

        /**
         * support inline string evaluation
         * tags: ["node:{$node.id()}", "node:{$node.id()}:comment_count"]
         */
        $op = preg_replace_callback('#\{([\$\@][^\}]+)\}#',
          fn($matches) => "' . " . $this->parseOperand($matches[1]) . " . '",
          $op);
        $op = preg_replace("#(^'' \\. )|( \\. ''\$)#", '', $op);

        return $op;
      }
      else {
        $op = preg_replace('#^@#', '', $op);
        $op = explode('.', $op);
        $source = array_shift($op);
        switch ($source) {
          case 'request':
            $source = "Drupal::request()->attributes->get('" . array_shift($op) . "')";
            break;
          case 'user':
            $source = "Drupal::currentUser()";
            break;
          case 'route':
            $source = "Drupal::routeMatch()->getRouteName()";
            break;
          case 'service':
            $service = array_shift($op);
            if ($service[0] === '{') {
              while (mb_substr($service, -1, 1) !== '}') {
                $service .= '.' . array_shift($op);
              }
              $service = trim($service, '{}');
            }
            $source = "Drupal::service('{$service}')";
            break;
        }
        array_unshift($op, $source);

        return implode('->', $op);
      }
    }
    if (is_array($op) && is_callable($op)) {
      [$class, $method] = $op;
      $class = trim($class, '\\');
      return "[{$class}::class, '{$method}']";
    }
    if (is_array($op)) {
      $output = [];
      $export_keys = array_values($op) != $op;
      foreach ($op as $key => $item) {
        $key = var_export($key, TRUE);
        $output[] = ($export_keys ? ($key . ' => ') : '') . $this->parseOperand($item, $space . '  ');
      }
      if (strlen(implode(', ', $output)) < 120) {
        return '[' . implode(', ', $output) . ']';
      }
      return "[\n$space  " . implode(",\n  $space", $output) . "\n" . $space . "]";

    }

    return var_export($op, TRUE);
  }

}
