<?php

namespace Drupal\gearbox\SerializableCallback;

use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RedirectController extends BaseSerializableCallback {

  protected array $namespaces = [
    RedirectResponse::class,
    Url::class,
  ];

  /**
   * @var string[]
   */
  private array $redirect;

  /**
   * RedirectController constructor.
   *
   * @param array|string $redirect
   */
  public function __construct($redirect) {
    parent::__construct();
    if (is_string($redirect)) {
      $redirect = ['route' => $redirect];
    }
    $redirect += ['code' => '302'];
    $this->redirect = $redirect;
  }

  /**
   * @return string
   */
  public function getFunctionCode(): string {
    $code = [];
    $code[] = "function {$this->getFunctionName()}(): RedirectResponse {";
    $method = 'fromRoute';
    $glue = ",\n    ";
    if (isset($this->redirect['uri'])) {
      $method = 'fromUri';
    }
    elseif (isset($this->redirect['input'])) {
      $method = 'fromUserInput';
    }
    $source = $this->redirect['input'] ?? $this->redirect['uri'] ?? $this->redirect['route'];
    $source = var_export($source, TRUE);
    $arguments = [];
    if (isset($this->redirect['parameters'])) {
      $arguments[] = $this->parseOperand($this->redirect['parameters']);
    }
    if (isset($this->redirect['options'])) {
      $arguments[] = $this->parseOperand($this->redirect['options']);
    }
    $arguments = $arguments ? ($glue . implode($glue, $arguments) . "\n  ") : '';
    $code[] = "  \$url = Url::{$method}({$source}{$arguments})->toString();";
    $code[] = "  return new RedirectResponse(\$url, {$this->redirect['code']});";
    $code[] = '}';

    return implode("\n", $code);
  }
}
