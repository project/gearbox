<?php

namespace Drupal\gearbox\SerializableCallback;

class ConditionalAlter extends BaseSerializableCallback {

  /** @var callable @var */
  protected $function;

  protected array $parameters;

  protected array $if;

  public function __construct(callable $function, array $parameters, array $if) {
    parent::__construct();
    $this->function = $function;
    $this->parameters = $parameters;
    $this->if = $if;
  }

  public function getFunctionCode(): string {
    $code = [];
    $code[] = "function {$this->getFunctionName()}(" . implode(', ', $this->parameters[0]) . ") {";
    $code[] = "  if (" . implode("\n    && ", $this->if) . ") {";
    $code[] = "    {$this->function}(" . implode(', ', $this->parameters[1]) . ");";
    $code[] = "  }";
    $code[] = "}";

    return implode("\n", $code);
  }

}
