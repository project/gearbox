<?php

namespace Drupal\gearbox\SerializableCallback;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ErrorController extends BaseSerializableCallback {

  protected array $namespaces = [
    HttpException::class,
  ];


  private int $code;

  /**
   * ErrorController constructor.
   *
   * @param int $code
   */
  public function __construct(int $code) {
    parent::__construct();
    $this->code = $code;
  }

  /**
   * @return string
   */
  public function getFunctionCode(): string {
    $code = [];
    $code[] = "function {$this->getFunctionName()}() {";
    $code[] = "  throw new HttpException({$this->code});";
    $code[] = "}";

    return implode("\n", $code);
  }

}
