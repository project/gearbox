<?php

namespace Drupal\gearbox\SerializableCallback;

class AppliesRule extends BaseSerializableCallback {

  private $rules = [];

  /**
   * AppliesRule constructor.
   *
   * @param array $rules
   */
  public function __construct(array $rules) {
    parent::__construct();
    if (!is_array($rules[0])) {
      $rules = [$rules];
    }
    if (!is_array($rules[0][0])) {
      $rules = [$rules];
    }
    $this->rules = $rules;
  }

  /**
   * @return string
   */
  public function getFunctionCode(): string {
    $code = [];
    $conditions = [];
    foreach ($this->rules as $i => $rule_set) {
      foreach ($rule_set as $j => $rule) {
        if ($rule[0][0] === '!' && !isset($rule[1])) {
          $rule[1] = $rule[2] = '';
        }
        if (!isset($rule[2])) {
          $rule[2] = '===';
        }
        if (!isset($rule[1])) {
          $rule[2] = '!==';
        }
        $conditions[$i][$j] = $this->generateOperation($rule[0], $rule[1] ?? NULL, $rule[2] ?? '===');
      }
    }
    $code[] = "function {$this->getFunctionName()}(): bool {";
    $code[] = "  return " . implode(PHP_EOL . "    || ",
        array_map(fn($conditions) => implode(' && ', $conditions), $conditions)) . ';';
    $code[] = '}';

    return implode(PHP_EOL, $code);
  }

  /**
   * @param $op1
   * @param $op2
   * @param string $operation
   *
   * @return string
   */
  protected function generateOperation($op1, $op2, string $operation): string {
    $op1 = $this->parseOperand($op1);
    if (!$operation) {
      return $op1;
    }
    $op2 = $this->parseOperand($op2);
    switch ($operation) {
      case 'in':
        return "in_array($op1, $op2, TRUE)";
      case 'not in':
        return "!in_array($op1, $op2, TRUE)";
      case 'call':
        return "call_user_func_array($op1, $op2)";
      case '===' :
        return "$op2 $operation $op1";
      default:
        return "$op1 $operation $op2";
    }
  }
}
