<?php

namespace Drupal\gearbox;

use Drupal\gearbox\SerializableCallback\SerializableCallbackInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension {

  public $callbacks = [];

  public $namespaces = [];

  /**
   * @return TwigFilter[]
   */
  public function getFilters(): array {
    return [
      new TwigFilter('export', [$this, 'export'], ['is_safe' => ['html']]),
      new TwigFilter('function', [$this, 'functionName'], ['is_safe' => ['html']]),
    ];
  }

  /**
   * @return TwigFunction[]
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('flush_callbacks', [$this, 'flushCallbacks'], ['is_safe' => ['html']]),
      new TwigFunction('flush_namespaces', [$this, 'flushNamespaces'], ['is_safe' => ['html']]),
      new TwigFunction('use', [$this, 'addNamespace'], ['is_safe' => ['html']]),
    ];
  }

  /**
   * @return string
   */
  public function flushCallbacks(): string {
    $output = implode("\n\n", $this->callbacks);
    $this->callbacks = [];

    return $output;
  }

  /**
   * @return string
   */
  public function flushNamespaces(): string {
    $output = implode("\n", array_map(fn($namespace) => "use {$namespace};", $this->namespaces));
    $this->namespaces = [];

    return $output;
  }

  /**
   * @param string $namespace
   */
  public function addNamespace(string $namespace) {
    $this->namespaces[$namespace] = $namespace;
  }

  /**
   * @param mixed $v
   * @param string $space
   *
   * @param bool $key
   *
   * @return string
   */
  public function export($v, string $space = '  ', $key = FALSE): string {
    if (is_object($v) && $v instanceof SerializableCallbackInterface) {
      $this->callbacks[$v->getFunctionName()] = $v->getFunctionCode();
      $this->namespaces += array_combine($v->getNamespaces(), $v->getNamespaces());
      return var_export($v->getFunctionName(), TRUE);
    }
    if (is_string($v) && is_callable($v) && strpos($v, '::')) {
      [$class, $method] = explode('::', $v);
      $class = trim($class, '\\');
      if (!$key) {
        return "[{$class}::class, '{$method}']";
      }
      else {
        return "{$class}::class . '::{$method}'";
      }
    }
    if (is_array($v) && is_callable($v)) {
      [$class, $method] = $v;
      $class = trim($class, '\\');
      return "[{$class}::class, '{$method}']";
    }
    if (is_bool($v) || is_null($v)) {
      return strtoupper(var_export($v, TRUE));
    }
    if (is_scalar($v)) {
      return var_export($v, TRUE);
    }
    if (is_array($v)) {
      $output = [];
      $export_keys = array_values($v) != $v;
      foreach ($v as $key => $item) {
        $key = $this->export($key, '', TRUE);
        $output[] = ($export_keys ? ($key . ' => ') : '') . $this->export($item, $space . '  ');
      }
      if (strlen(implode(', ', $output)) < 120) {
        return '[' . implode(', ', $output) . ']';
      }
      return "[\n$space  " . implode(",\n  $space", $output) . "\n" . $space . ']';
    }
  }

  public function functionName($function) {
    if (is_string($function)) {
      return $function;
    }
    if (is_object($function) && $function instanceof SerializableCallbackInterface) {
      $this->callbacks[$function->getFunctionName()] = $function->getFunctionCode();
      $this->namespaces += array_combine($function->getNamespaces(), $function->getNamespaces());
      return $function->getFunctionName();
    }
  }

  /**
   * @return string
   */
  public function getName(): string {
    return __CLASS__;
  }
}
