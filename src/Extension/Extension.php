<?php

namespace Drupal\gearbox\Extension;

use Drupal\gearbox\Linker;

class Extension extends \Drupal\Core\Extension\Extension {

  /**
   * @return bool
   */
  public function load(): bool {
    $info = $this->info ?? \Drupal::service('extension.list.' . $this->type)->getExtensionInfo($this->getName());
    if ($this->getName() === 'gearbox' || in_array('gearbox', $info["dependencies"] ?? [])) {
      new Linker($this->getName(), $this->type);
    }
    return parent::load();
  }
}
