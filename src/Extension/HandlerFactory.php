<?php

namespace Drupal\gearbox\Extension;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Extension\ThemeHandler;

class HandlerFactory {

  /**
   * @param ThemeHandler $handler
   *
   * @return ThemeHandler
   */
  static public function themeAlter(ThemeHandler $handler): ThemeHandler {
    foreach ($handler->listInfo() as $theme_original) {
      if (in_array('gearbox', $theme_original->info["dev_dependencies"] ?? [])) {
        $theme = new Extension(DRUPAL_ROOT, $theme_original->getType(), $theme_original->getPathname(), $theme_original->getExtensionFilename());
        foreach (get_object_vars($theme_original) as $k => $v) {
          $theme->$k = $v;
        }
        $theme->dependencies = ['gearbox'];
        $handler->addTheme($theme);
      }
    }
    return $handler;
  }

  /**
   * @param ModuleHandler $handler
   *
   * @return ModuleHandler
   */
  static public function moduleAlter(ModuleHandler $handler): ModuleHandler {
    $module_list = $handler->getModuleList();
    foreach ($module_list as $name => $module_original) {
      $module_list[$name] = new Extension(DRUPAL_ROOT, $module_original->getType(), $module_original->getPathname(), $module_original->getExtensionFilename());
    }
    $handler->setModuleList($module_list);
    return $handler;
  }
}
