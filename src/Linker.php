<?php

namespace Drupal\gearbox;

use Drupal;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\PhpStorage\PhpStorageFactory;
use Drupal\gearbox\DocBlock\DocBlock;
use Drupal\gearbox\SerializableCallback\ConditionalAlter;
use Drush\Commands\DrushCommands;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RecursiveRegexIterator;
use ReflectionParameter;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionClass;
use RegexIterator;
use Twig_Environment;
use Twig_Loader_Filesystem;

class Linker {

  protected static $extensions = [];

  protected string $extension;

  protected string $type;

  protected array $implementations = [];

  protected array $events = [];

  protected array $code = [];

  protected array $required = [];

  /**
   * Linker constructor.
   *
   * @param string $extension
   * @param string $type
   */
  public function __construct(string $extension, string $type = 'module') {
    if (in_array($extension, $this::$extensions)) {
      return;
    }
    $this::$extensions[] = $extension;
    $this->extension = $extension;
    $this->type = $type;
    $storage = PhpStorageFactory::get('gearbox');
    if (!$storage->exists($extension)) {
      $this->scanImplementations();
      $this->sortImplementations();
      $this->generateCode();
      $this->code = array_map('trim', $this->code);
      $this->code = array_filter($this->code);
      $storage->save($extension, implode("\n\n", $this->code) . "\n");
    }

    require_once $storage->getFullPath($extension);
  }

  protected function scanImplementations(): void {
    $DrushCommands = DrushCommands::class;
    foreach ($this->getFiles() as $file) {
      try {
        $code = file_get_contents($file);
        preg_match('/namespace\s+(?<namespace>[^\s]+)\s*[;{]/i', $code, $matches);
        $namespace = $matches['namespace'] ?? '';
        preg_match('/\n\s*class\s+(?<class>[^\s]+)/i', $code, $matches);
        $class = $matches['class'] ?? '';
        if ($class && $namespace) {
          $class = "{$namespace}\\{$class}";
        }
        if ($class && class_exists($class)
          && ($reflection = new ReflectionClass($class))
          && $reflection->isInstantiable()
          && !($reflection->isSubclassOf(DrushCommands::class))) {
          $functions = array_map(
            fn($method) => $method->name,
            $reflection->getMethods(ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_STATIC));
        }
        elseif (!$class) {
          preg_match_all('/function\s+(?<function>[^\s]+)\s*\(/i', $code, $matches);
          $functions = $matches['function'];
        }
        else {
          continue;
        }
        require_once $file;
        foreach ($functions as $function) {
          if (!$class && $namespace) {
            $function = $namespace . '\\' . $function;
          }
          if (!$class && function_exists($function)) {
            $function_info = new ReflectionFunction($function);
          }
          elseif ($class && method_exists($class, $function)) {
            $function_info = new ReflectionMethod($class, $function);
          }
          else {
            continue;
          }
          $doc_block = new DocBlock($function_info->getDocComment() ?: '');
          $function = $class ? "$class::$function" : $function;
          $this->processDefinition($file, $doc_block, $class, $function, $function_info);
        }
      } catch (\Exception $e) {
      }
    }
    $this->required = array_unique($this->required);
  }

  /**
   *
   * @return \Generator|string[]
   */
  protected function getFiles(): \Generator {
    $project_dir = drupal_get_path($this->type, $this->extension);
    if (is_file($file = "{$project_dir}/{$this->extension}.{$this->type}")) {
      yield $file;
    }
    foreach ($this->getProjectSubDirs($project_dir) as $dir) {
      $Directory = new RecursiveDirectoryIterator($dir);
      $Directory->setFlags(FilesystemIterator::FOLLOW_SYMLINKS);
      $Iterator = new RecursiveIteratorIterator($Directory);
      $Regex = new RegexIterator($Iterator, '/^.+\.(inc|php)$/i', RecursiveRegexIterator::GET_MATCH);
      foreach ($Regex as $file) {
        yield $file[0];
      }
    }
  }

  /**
   * @param string $project_dir
   *
   * @return \Generator
   */
  protected function getProjectSubDirs(string $project_dir): \Generator {
    if (is_dir($dir = $project_dir . '/src')) {
      yield $dir;
    }
    if (is_dir($dir = $project_dir . '/includes')) {
      yield $dir;
    }
  }

  /**
   * @param $doc_block
   * @param $class
   * @param $function
   * @param $file
   * @param $function_info
   */
  protected function processDefinition($file, $doc_block, $class, $function, $function_info): void {
    if ($doc_block->event) {
      $this->events[] =
        [
          'name'     => $doc_block->event,
          'listener' => $function,
          'priority' => (int) $doc_block->priority,
        ];
      if (!$class) {
        $this->required[] = $file;
      }
    }
    if ($doc_block->hook) {
      if (!$class) {
        $this->required[] = $file;
      }
      if ($doc_block->hook === 'callback' && !$doc_block->return) {
        $code = implode('', array_slice(file($file), $function_info->getStartLine() - 1, 1 + $function_info->getEndLine() - $function_info->getStartLine()));
        if (strpos($code, 'return') === FALSE) {
          $doc_block->return = 'NULL';
        }
      }
      if ($doc_block->hook === 'form_alter' && $doc_block->form_id) {
        $if = (array) $doc_block->if;
        array_unshift($if, "preg_match('/^({$doc_block->form_id})$/', \$form_id)");
        $doc_block->if = $if;
      }
      if ($doc_block->if) {
        $function = new ConditionalAlter($function, $this->convertParametersToString($function_info->getParameters()), (array) $doc_block->if);
      }
      $this->implementations[$doc_block->hook][] = [
        'class'     => $class,
        'function'  => $function,
        'doc_block' => $doc_block,
        'arguments' => $this->convertParametersToString($function_info->getParameters()),
      ];
    }
  }

  /**
   * @param ReflectionParameter[] $parameters
   *
   * @return array
   */
  private function convertParametersToString(array $parameters): array {
    return [
      array_map(fn(ReflectionParameter $parameter) => ($parameter->hasType() ? ($parameter->getType()
            ->getName() . ' ') : '')
        . ($parameter->isPassedByReference() ? '&' : '')
        . '$' . $parameter->getName()
        . ($parameter->isDefaultValueAvailable() ? (' = ' . var_export($parameter->getDefaultValue(), TRUE)) : '')
        , $parameters),
      array_map(fn(ReflectionParameter $parameter): string => '$' . $parameter->getName(), $parameters),
    ];
  }

  protected function sortImplementations(): void {
    foreach ($this->implementations as $k => $group) {
      foreach ($group as $i => $implementation) {
        $group[$i]['weight'] = $implementation['doc_block']->weight ?? 0;
      }
      uasort($group, [SortArray::class, 'sortByWeightElement']);
      $this->implementations[$k] = $group;
    }
  }

  protected function generateCode(): void {
    if ($this->events) {
      $this->code[] = $this->generateEvents();
    }
    $implementations = array_diff(array_keys($this->getImplementations()), [
      'form_alter',
      'callback',
      'generator',
    ]);
    $need_hook_helper = FALSE;
    if ($implementations) {
      foreach ($implementations as $hook) {
        if ($this->hookIsAlter($hook)) {
          $this->code[] = $this->generateHookAlter($hook);
        }
        elseif ($this->hookIsFast($hook)) {
          $this->code[] = $this->generateHookFast($hook);
        }
        else {
          $this->code[] = $this->generateHook($hook);
          $need_hook_helper = $need_hook_helper || (count($this->getImplementations($hook)) > 1);
        }
      }
    }
    if ($need_hook_helper) {
      $this->code[] = $this->generateHookInvoke();
    }
    if ($this->getImplementations('form_alter')) {
      $this->code[] = $this->generateFormsAlter();
    }

    foreach ($this->getImplementations('callback') as $implementation) {
      $this->code[] = $this->generateCallback($implementation);
    }

    foreach ($this->getImplementations('generator') as $implementation) {
      $this->code[] = $this->callGenerator($implementation);
    }

    $this->code[] = $this->template('at_end');
    array_unshift($this->code, $this->template('at_start', ['required' => $this->required]));
  }

  /**
   * @return string
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  private function generateEvents(): string {
    return $this->template('events', ['events' => $this->events]);
  }

  /**
   * @param string $template
   * @param array $data
   *
   * @return string
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  protected function template(string $template, array $data = []): string {
    return $this->twig()->render($template . '.twig', array_merge($data, [
      'module' => $this->extension,
    ]));
  }

  /**
   * @return Twig_Environment
   */
  protected function twig(): Twig_Environment {
    static $twig = NULL;
    if (!$twig) {
      $twig = new Twig_Environment(new Twig_Loader_Filesystem(drupal_get_path('module', 'gearbox') . '/templates'));
      $twig->addExtension(new TwigExtension());
    }

    return $twig;
  }

  /**
   * @param null|string $hook
   *
   * @return array
   */
  private function getImplementations(?string $hook = NULL): array {
    return $hook ? (isset($this->implementations[$hook]) ? $this->implementations[$hook] : []) : $this->implementations;
  }

  /**
   * @param string $hook
   *
   * @return bool
   */
  private function hookIsAlter(string $hook): bool {
    return preg_match('/_alter$/', $hook)
      || preg_match('/^preprocess_/', $hook)
      || $this->getImplementations($hook)[0]['doc_block']->alter();
  }

  /**
   * @param string $hook
   *
   * @return string
   * @throws \ReflectionException
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  private function generateHookAlter(string $hook): string {
    $implementations = $this->getImplementations($hook);
    $params_count = 1;
    foreach ($implementations as $implementation) {
      $params_count = max($params_count, count($implementation['arguments'][0]));
    }
    return $this->template('hook_alter', [
      'hook'            => $hook,
      'args'            => $this->convertParametersToString(array_slice(
        (new ReflectionMethod(Drupal::moduleHandler(), 'alter'))->getParameters(), 1, $params_count)),
      'implementations' => $implementations,
    ]);
  }

  /**
   * @param $hook
   *
   * @return bool
   */
  private function hookIsFast(string $hook): bool {
    foreach ($this->getImplementations($hook) as $implementation) {
      if (!$implementation['doc_block']->fast()) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * @param string $hook
   *
   * @return string
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  private function generateHookFast(string $hook): string {
    $suffix = '_' . mt_rand(10000, 99999);
    $this->extension .= $suffix;
    $code = 'use ' . NestedArray::class . ';';
    $code .= $this->generateHookInvoke(FALSE) . $this->generateHook($hook);
    $code .= "return {$this->extension}_{$hook}();";
    $this->extension = str_replace($suffix, '', $this->extension);
    try {
      $val = eval($code);
      return $this->template('hook_fast', ['hook' => $hook, 'val' => $val]);
    } catch (\Exception $e) {
      return '/* ' . $e->getMessage() . '*/' . "\n\n" . $this->generateHook($hook);
    }
  }

  /**
   * @param bool $use
   *
   * @return string
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  private function generateHookInvoke($use = TRUE): string {
    return $this->template('hook_invoke', [$use => $use]);
  }

  /**
   * @param string $hook
   *
   * @return string
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  private function generateHook(string $hook): string {
    return $this->template('hook', [
      'hook'            => $hook,
      'implementations' => $this->getImplementations($hook),
    ]);
  }

  /**
   * @return string
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  private function generateFormsAlter(): string {
    return $this->template('form_alter', ['implementations' => $this->getImplementations('form_alter')]);
  }

  /**
   * @param array $implementation
   *
   * @return string
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  private function generateCallback(array $implementation): string {
    return $this->template('callback',
      [
        'function'       => $this->generateCallbackName($implementation['function']),
        'return'         => 'NULL' !== strtoupper($implementation['doc_block']->return) ? 'return ' : '',
        'implementation' => $implementation,
      ]);
  }

  /**
   * @param string $function
   *
   * @return string
   */
  public static function generateCallbackName(string $function): string {
    return '_f__' . preg_replace('/\W/', '__', ltrim($function, '\\'));
  }

  /**
   * @param array $implementation
   *
   * @return string
   */
  private function callGenerator(array $implementation): string {
    return $implementation['function']();
  }

}

