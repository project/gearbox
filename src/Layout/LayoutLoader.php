<?php

namespace Drupal\gearbox\Layout;

use Drupal;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Discovery\YamlDiscovery;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Render\Element;
use Drupal\gearbox\Linker;
use Drupal\gearbox\SerializableCallback\AppliesRule;

class LayoutLoader {

  /**
   * @param array $page
   *
   * @return array
   */
  static function preRender(array $page): array {
    $page['#logo'] = theme_get_setting('logo');
    $page['#breadcrumb'] = $page['content']['main_content']['#breadcrumb']
      ?? $page['content']['system_main']['#breadcrumb']
      ?? Drupal::service('breadcrumb')->build(Drupal::routeMatch());

    $manager = Drupal::service('plugin.manager.menu.local_task');
    if ($tabs = array_filter(
      array_map(fn($tabs) => count(Element::getVisibleChildren($tabs)) > 1 ? $tabs : NULL,
        [
          '#primary'   => $manager->getLocalTasks(Drupal::routeMatch()->getRouteName(), 0)['tabs'] ?? [],
          '#secondary' => $manager->getLocalTasks(Drupal::routeMatch()->getRouteName(), 1)['tabs'] ?? [],
        ]))) {
      $page['tabs'] = $tabs + ['#theme' => 'menu_local_tasks'];
    }

    return $page;
  }

  /**
   * @hook gearbox_layout
   * @fast
   *
   * @return array
   */
  static function LayoutVariant(): array {
    $layouts = [];
    $discovery = new YamlDiscovery('themes', Drupal::service('module_handler')
        ->getModuleDirectories() + Drupal::service('theme_handler')
        ->getThemeDirectories());
    foreach ($discovery->findAll() as $info) {
      foreach ($info['layouts'] ?? [] as $name => $layout) {
        if (isset($layout['applies']) && !is_callable($layout['applies'])) {
          $layout['applies'] = new AppliesRule($layout['applies']);
        }
        $layouts['layout--' . $name] = $layout;
      }
    }
    uasort($layouts, [SortArray::class, 'sortByWeightElement']);

    return $layouts;
  }

  /**
   * @hook theme
   * @fast
   *
   * @return array
   */
  static function LayoutTheme(): array {
    $themes = [];
    $info = Drupal::service('module_handler')
        ->getModuleList() + Drupal::service('theme_handler')
        ->listInfo();
    $dirs = array_map(fn(Extension $info) => $info->getPath(), $info);
    $discovery = new YamlDiscovery('themes', $dirs);
    foreach ($discovery->findAll() as $extension => $data) {
      foreach ($data['layouts'] ?? [] as $name => $theme) {
        unset($theme['weight'], $theme['applies']);
        if (!isset ($theme['variables'])) {
          $theme['render element'] = $theme['render element'] ?? 'page';
        }
        $theme['path'] = $dirs[$extension] . '/' . ($theme['path'] ?? 'templates');
        $theme['template'] = $theme['template'] ?? 'layout--' . $name;
        $theme += [
          'pattern'                       => FALSE,
          'preprocess functions'          => [],
          'override preprocess functions' => TRUE,
        ];
        foreach ($theme['preprocess functions'] as $k => $function) {
          if (strpos($function, '::')) {
            $theme['preprocess functions'][$k] = Linker::generateCallbackName($function);
          }
        }
        $themes['layout--' . $name] = $theme;
      }
    }

    return $themes;
  }

  /**
   * @hook preprocess_html
   *
   * @if !Drupal::service('router.admin_context')->isAdminRoute()
   * @if empty($html['page']['#theme'])
   *
   * @param $html
   */
  static function preprocess_html(&$html) {
    foreach (Drupal::moduleHandler()->invoke('gearbox', 'gearbox_layout') as $name => $variant) {
      if (!isset($variant['applies']) || $variant['applies']()) {
        $html['page']['#theme'] = $name;
        $html['page']['#pre_render'][] = [static::class, 'preRender'];
        break;
      }
    }
  }

}
