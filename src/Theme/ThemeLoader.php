<?php

namespace Drupal\gearbox\Theme;

use Drupal\Core\Discovery\YamlDiscovery;
use Drupal\Core\Extension\Extension;
use Drupal\gearbox\Linker;

class ThemeLoader {

  /**
   * @hook theme
   * @fast
   *
   * @return array
   */
  public static function load(): array {
    $themes = [];
    $info = \Drupal::service('module_handler')
        ->getModuleList() + \Drupal::service('theme_handler')
        ->listInfo();
    $dirs = array_map(fn(Extension $info) => $info->getPath(), $info);
    $discovery = new YamlDiscovery('themes', $dirs);
    foreach ($discovery->findAll() as $extension => $info) {
      foreach ($info['themes'] ?? [] as $name => $theme) {
        if (!isset($theme['variables'])) {
          $theme['render element'] = $theme['render element'] ?? 'scope';
        }
        $theme['path'] = $dirs[$extension] . '/' . ($theme['path'] ?? 'templates');
        $theme['template'] ??= $name;
        $theme += [
          'pattern'                       => FALSE,
          'preprocess functions'          => [],
          'override preprocess functions' => TRUE,
        ];
        foreach ($theme['preprocess functions'] as $k => $function) {
          if (strpos($function, '::')) {
            $theme['preprocess functions'][$k] = Linker::generateCallbackName($function);
          }
        }
        $themes[$name] = $theme;
      }
    }

    return $themes;
  }
}
