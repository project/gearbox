<?php

namespace Drupal\gearbox\Commands;

use Drupal\Core\PhpStorage\PhpStorageFactory;
use Drush\Commands\DrushCommands;

class GearboxCommands extends DrushCommands {

  /**
   * @hook on-event cache-clear
   */
  public function cacheClear(&$types, $include_bootstrapped_types) {
    if ($include_bootstrapped_types) {
      $types['gearbox'] = [$this, 'purgeStorage'];
    }
  }

  public function purgeStorage() {
    PhpStorageFactory::get('gearbox')->deleteAll();
  }
}
