<?php

namespace Drupal\gearbox\Routing;

use Drupal\Component\Discovery\YamlDiscovery;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\gearbox\SerializableCallback\AppliesRule;
use Drupal\gearbox\SerializableCallback\CacheController;
use Drupal\gearbox\SerializableCallback\ErrorController;
use Drupal\gearbox\SerializableCallback\RedirectController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * @hook route
   * @fast
   *
   * @return array
   * @throws \ReflectionException
   */
  public static function route(): array {
    $routes = [];
    $alters = [];
    $discovery = new YamlDiscovery('routing', \Drupal::service('module_handler')
      ->getModuleDirectories());
    foreach ($discovery->findAll() as $all_routes) {
      foreach ($all_routes as $name => $route) {
        if (isset($route['options']['cache']) || isset($route['defaults']['_response']) || isset($route['options']['alter'])) {
          $route['defaults']['_controller_wrapper'] =
            (new CacheController($route['options']['cache'] ?? [], $route['defaults']['_controller'] ?? '', $route['defaults']['_response'] ?? []));
          $routes[$name] = $route;
        }
        if (isset($route['options']['redirect'])) {
          $route['defaults']['_controller_wrapper'] = new RedirectController($route['options']['redirect']);
          $routes[$name] = $route;
        }
        if (isset($route['options']['error'])) {
          $route['defaults']['_controller_wrapper'] = new ErrorController($route['options']['error']);
          $routes[$name] = $route;
        }
        if (isset($route['options']['condition']) && !is_callable($route['options']['condition'])) {
          $route['options']['condition_compiled'] = new AppliesRule($route['options']['condition']);
          $routes[$name] = $route;
        }
        if (isset($route['options']['alter'])) {
          $alters[$route['options']['alter']][$name] = ['weight' => $route['options']['weight'] ?? 0];
        }

      }
    }
    foreach ($alters as $name => $variants) {
      uasort($variants, [SortArray::class, 'sortByWeightElement']);
      $alters[$name] = array_keys($variants);
    }

    return ['routes' => $routes, 'alters' => $alters];
  }

  /**
   * @event kernel.controller
   * @priority 9999
   *
   * @param FilterControllerEvent $event
   */
  public static function onRequest(FilterControllerEvent $event) {
    ['routes' => $routes, 'alters' => $alters] = \Drupal::moduleHandler()->invoke('gearbox', 'route');
    $request = $event->getRequest();
    $route_name = $request->attributes->get('_route');
    if ($wrapper = $routes[$route_name]['defaults']['_controller_wrapper'] ?? '') {
      $event->setController($wrapper);
      $request->attributes->set('_controller_wrapper', $wrapper);
    }
    if ($alters = $alters[$route_name] ?? NULL) {
      foreach ($alters as $variant) {
        $route = $routes[$variant];
        if (!isset($route['options']['condition']) || $route['options']['condition_compiled']()) {
          $event->setController($route['defaults']['_controller_wrapper']);
          $request->attributes->set('_controller_wrapper', $route['defaults']['_controller_wrapper']);
          $request->attributes->set('_controller', $route['defaults']['_controller'] ?? '');
          $request->attributes->set('_route', $variant);
          $request->attributes->set('_route_original', $route_name);
          break;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $routes = \Drupal::moduleHandler()->invoke('gearbox', 'route')['routes'];
    foreach ($routes as $name => $route) {
      if (isset($route["defaults"]["_controller_wrapper"]) && empty($route["defaults"]["_controller"])) {
        $collection->get($name)
          ->addDefaults(['_controller' => $route["defaults"]["_controller_wrapper"]]);
      }
      if (isset($route['options']['alter'])) {
        $collection->remove($name);
      }
    }
  }

}
